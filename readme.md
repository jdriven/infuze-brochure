## Introduction

This is a template project to create Infuze brochures.  
If you want to use this template, please create a branch for your purpose.

## Getting started

https://plantuml.com/starting

## References

### PDF Theme guide

https://github.com/asciidoctor/asciidoctor-pdf/blob/main/docs/theming-guide.adoc

### Asciidoc

https://docs.asciidoctor.org/asciidoc/latest/syntax-quick-reference/

### Modelling with Ricardo Niepel's C4 PlantUML syntax

https://github.com/plantuml-stdlib/C4-PlantUML/tree/master/samples
